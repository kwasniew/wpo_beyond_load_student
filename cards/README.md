# Dynamic layers

```
http-server .
```

It should start a server on http://localhost:8080

We've got an app used for swiping cards left or right.
The cards disappear when we move them far enough from the original location.
If we don't move them far enough they snap back into original location.

## Observing paint flashes

Enable paint flashing in Chrome Dev Tools and move some cards.

## Solving paint flashes in CSS

Add a CSS property to remove paint flashes.

## More cards = more layers

Uncomment this line of code:
```javascript
// jokes();
```

It will start adding a new card every second potentially building up to hundreds of cards.

Enable the layers view in Chrome Dev Tools. How many layers are we creating?

## Adding layers on demand

Your task is to replace CSS property with JS code that will add layers on demand
when we start moving the card. Play around with the code to find the right place
to add one new line of code.

## Removing unused layers

When the card snaps back to its original location we should remove a layer.
Your task is to figure out how to do it.
It should be one line of code.