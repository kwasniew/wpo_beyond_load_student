# Smooth side nav

## Getting to know the app

Run the app with any HTTP server.

E.g.
```
npm i http-server -G
http-server .
```

It should start a server on http://localhost:8080

Click on the hamburger menu to see how the menu opens. 
Clicking on the x button should close the menu.
Also, clicking anywhere outside the menu should close it.

One thing that may bother our users is grey area change that lacks any animation.

## Getting to know the code

All the code is in index.html file.
Let's analyze it together.

## Tasks

* Eliminate Paint Flashing. Only small area around button can trigger Paint Flashing.
* Fix opacity changes so that they are synchronized with the sliding menu. 

To measure how the side menu performs when opened/closed you can see Performance Timeline and
Paint Flashing in Chrome Dev Tools.
There should be no Layout activity in the timeline.