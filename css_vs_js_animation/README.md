# CSS vs JS animation

This example compares:
* pure JS animations with setTimeout and requestAnimationFrame and CSS animations
* layout property animations (left) vs composite property animations (translate)

```
http-server .
```

It should start a server on http://localhost:8080

## Busy main thread 

There's a slow() function that occupies main thread.
Which animated box is not affected?

Disable JS code with slow function.
Enable CPU slowdown in Performance tab.

Which boxes are affected?

Enable Paint Flashing.

Which boxes are green?

## Performance profiles

Leave only one box at a time and comment the rest.
Record performance profile of each animation.